$(document).ready(function() {

  var errorNotify = function(msg) {
    $('.notify').remove();
    $('body').append('<div class="notify error"><a id="close">x</a>' + msg + "</div>");

    $('#close').click(function() {
      $('.notify').remove();
    });
  };

  var successNotify = function(msg) {
    $('.notify').remove();
    $('body').append('<div class="notify success"><a id="close">x</a>' + msg + "</div>");

    $('#close').click(function() {
      $('.notify').remove();
    });
  };

  var connect = function() {
    if (!socket || socket.disconnected) {
      socket = io({forceNew: true});
    }
  };

  var socket;


  var nick = "";
  var currentRoom;

  // begin loginStage
    var nickTextField = $('#nick');

    connect();
    $('#login #send').click(function(e) {
      e.preventDefault();
      if (nickTextField.val().trim() === "") {
        errorNotify("Nick jest wymagany");
      } else {
        socket.emit('login', nickTextField.val());
      }
    });

    socket.on('validate login', function(response) {
      if (response.status) {
        $('.notify').remove();
        nick = nickTextField.val();
        $('#login').addClass('hidden');
        $('#lobby').removeClass('hidden');
        successNotify("Zalogowano pomyślnie. Witaj " + nick);
      } else {
        errorNotify(response.msg);
      }
    });
  // end loginStage

  // start lobbyStage

    var roomName = $('#roomName');
    var rooms = $('.rooms ul');
    var people = $('.people ul');


    $('#addRoom').click(function() {
      $('#formRoom').toggleClass('hidden');
    });

    $('#createRoom').click(function(e) {
      e.preventDefault();
      if (roomName.val().trim() === "") {
        errorNotify("Nazwa jest wymagana");
      } else {
        socket.emit('create room', roomName.val());
      }
    });

    $('#logout').click(function() {
      $("#lobby").addClass('hidden');
      $('#login').removeClass('hidden');
      socket.emit('logout');

      successNotify("Wylogowano pomyślnie");
    });

    socket.on('rooms', function(roomsParam) {
      rooms.empty();
      $('.rooms h2 span').html("(" + roomsParam.length +")");
      roomsParam.map(function(room) {
        rooms.append('<li id="' + room.name.replace(/ /g, '-') + '">' + room.name + " ("+ room.people.length + ")</li>");
        $('#' + room.name.replace(/ /g, '-')).click(function() {
          currentRoom = room.name;
          socket.emit('join room', room.name);
        });
      });
    });

    // ludzie w lobby
    socket.on('people', function(peopleParam) {
      people.empty();
      $('.people h2 span').html("(" + peopleParam.length + ")");
      peopleParam.sort();
      peopleParam.filter(function(person) {
       people.append("<li>" + person + "</li>");
      });
    });

    // dostaje odpowiedź czy utworzył pokój
    socket.on('validate create room', function(response) {
      if (response.status) {
        currentRoom = roomName.val();
        roomName.val("");
        $('#lobby').addClass('hidden');
        $('#room').removeClass('hidden');
        successNotify("Stworzono pokój " + currentRoom);
        setUpRoomStage();
      } else {
        errorNotify(response.msg);
      }
    });
    
    socket.on('validate join room', function(response) {
      if (response.status) {
        successNotify("Dołączyłeś do pokoju " + currentRoom);
        $('#lobby').addClass("hidden");
        $('#room').removeClass("hidden");
        setUpRoomStage();
      }
    });

  // end lobbyStage

  // begin roomStage
    var setUpRoomStage = function() {
      $('#room header h2').html(currentRoom);
      socket.emit('get history');
      socket.emit('get room users');
    };

    $('#leaveRoom').click(function() {
      socket.emit('leave room');
    });

    $('#room button').click(function(e) {
      e.preventDefault();
      var msg = $('#message');
      if (msg.val().trim() === "") {
        errorNotify("Wiadomość jest wymagana");
      } else {
        socket.emit('message', msg.val());
      }
      msg.val('');
    });

    socket.on('message', function(data) {
      $('.padding').append('<div>' + data.sender + ": " + data.msg + "</div>");
      $("#text").scrollTop($("#text")[0].scrollHeight);
    });

    socket.on('history', function(data) {
      data.filter(function(el) {
        $('.padding').append('<div><i>' + new Date(el.timestamp).toLocaleString() + ' - ' + el.sender + ": " + el.msg + "</i></div>");
      });
      $("#text").scrollTop($("#text")[0].scrollHeight);
    });

    socket.on('new user', function(nick) {
      $('.padding').append('<div><i>' + nick + ' dołączył się</i></div>');
    });

    var roomUsers = $('#roomUser ul');

    socket.on('room users', function(data) {
      roomUsers.empty();
      data.sort();
      data.filter(function(person) {
        roomUsers.append("<li>" + person + "</li>");
      });
      console.log(data);
    });

    socket.on('validate leave room', function(response) {
      if (response.status) {
        $('.padding').empty();
        $('#message').empty();
        roomUsers.empty();
        $('#room header h2').empty();
        $('#room').addClass('hidden');
        $('#formRoom').addClass('hidden');
        $('#lobby').removeClass('hidden');
      }
    });
  // end roomStage
});
