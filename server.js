var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var less = require('less-middleware');

app.use(less(__dirname + "/public"));
app.use(express.static(__dirname + "/public"));
app.use('/bower_components', express.static(__dirname + "/bower_components"));

var nicksInUse = [];
var history = [];
var rooms = [];

function Room(name) {
  this.name = name;

  this.people = [];
}


io.on("connection", function(socket) {
  socket.on('disconnect', function() {
    if (socket.nick) {
      var index = nicksInUse.indexOf(socket.nick);
      nicksInUse.splice(index, 1);
      if (socket.roomName) {
        for (var i in rooms) {
          if (rooms[i].name === socket.roomName) {
            index = rooms[i].people.indexOf(socket.nick);
            rooms[i].people.splice(index,1);
            if (rooms[i].people.length === 0) {
              rooms.splice(i, 1);
            } else {
              io.sockets.in(socket.roomName).emit('room users', rooms[i].people);
            }
          }
        }
        io.sockets.emit('rooms', rooms);
      }
      io.sockets.emit('people', nicksInUse);
    }
  });

  socket.on('logout', function() {
    if (socket.nick) {
      var index = nicksInUse.indexOf(socket.nick);
      nicksInUse.splice(index, 1);
      if (socket.roomName) {
        socket.leave(socket.roomName);
        for (var i in rooms) {
          if (rooms[i].name === socket.roomName) {
            index = rooms[i].people.indexOf(socket.nick);
            rooms[i].people.splice(index,1);
            if (rooms[i].people.length === 0) {
              rooms.splice(i, 1);
            } else {
              io.sockets.in(socket.roomName).emit('room users', rooms[i].people);
            }
          }
        }
        delete socket.roomName;
        socket.broadcast.emit('rooms', rooms);
      }
      delete socket.nick;
      socket.broadcast.emit('people', nicksInUse);
    }
  });

  socket.on('login', function(nick) {
    if (nicksInUse.indexOf(nick) > -1) {
      socket.emit('validate login', {status: false, msg: "Nick jest w użyciu"});
    } else {
      nicksInUse.push(nick);
      socket.nick = nick;
      socket.emit('validate login', {status: true});
      socket.emit('rooms', rooms);
      io.sockets.emit('people', nicksInUse);
    }
  });

  socket.on('create room', function(roomName) {
    for (var i in rooms) {
      if (rooms[i].name === roomName) {
        return socket.emit('validate create room', {status: false, msg: "Pokój w użyciu"});
      }
    }
    var newRoom = new Room(roomName);
    newRoom.people.push(socket.nick);
    rooms.push(newRoom);
    socket.roomName = roomName;
    socket.join(roomName);
    io.sockets.emit('rooms', rooms);
    socket.emit('validate create room', {status: true});
  });

  socket.on('join room', function(roomName) {
    for (var i in rooms) {
      if (rooms[i].name === roomName) {
        rooms[i].people.push(socket.nick);
        io.sockets.in(roomName).emit('room users', rooms[i].people);
        break;
      }
    }
    socket.roomName = roomName;
    socket.join(roomName);
    io.sockets.emit('rooms', rooms);
    socket.to(roomName).broadcast.emit('new user', socket.nick);
    socket.emit('validate join room', {status: true});
  });

  socket.on('leave room', function() {
    for (var i in rooms) {
      if (rooms[i].name === socket.roomName) {
        var index = rooms[i].people.indexOf(socket.nick);
        rooms[i].people.splice(index, 1);
        if (rooms[i].people.length === 0) {
          rooms.splice(i, 1);
        } else {
          io.sockets.in(socket.roomName).emit('room users', rooms[i].people);
        }
        break;
      }
    }
    io.sockets.emit('rooms', rooms);
    socket.leave(socket.roomName);
    delete socket.roomName;
    socket.emit('validate leave room', {status: true});
  });

  socket.on('get rooms', function() {
    socket.emit('rooms', rooms);
  });

  socket.on('get people', function() {
    socket.emit('people', nicksInUse);
  });

  socket.on('message', function(msg) {
    history.push({room: socket.roomName, sender: socket.nick, msg: msg, timestamp: new Date()});
    io.sockets.in(socket.roomName).emit('message', {sender: socket.nick, msg: msg});
  });

  socket.on('get history', function() {
    var toSend = [];
    for (var i in history) {
      if (history[i].room === socket.roomName) {
        toSend.push(history[i]);
      }
    }
    socket.emit('history', toSend);
  });

  socket.on('get room users', function() {
    for (var i in rooms) {
      if (rooms[i].name === socket.roomName) {
        return socket.emit('room users', rooms[i].people);
      }
    }
  });
});


server.listen(process.env.PORT || 3000, function() {
  console.log("Serwer został uruchomiony");
});
